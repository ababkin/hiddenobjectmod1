﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ComicsManager : MonoBehaviour {

    public GameObject[] ComicsImage;

    void Awake() {
        transform.GetChild(3).gameObject.SetActive(true);
    }

    void Start() {
        StartCoroutine(Comics());
    }

    IEnumerator Comics() {
        switch (GlobalData.NumberLevel) {
            case 0:
                ComicsImage[0].SetActive(true);
                ComicsImage[0].transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1f);
                ComicsImage[0].transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1f);
                ComicsImage[0].transform.GetChild(1).gameObject.SetActive(true);
                yield return new WaitForSeconds(2f);
                ComicsImage[0].transform.GetChild(2).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ComicsImage[0].transform.GetChild(3).gameObject.SetActive(true);
                yield return new WaitForSeconds(2f);
                transform.GetChild(3).GetComponent<Animator>().Play("Close_Close_Panel");
                yield return new WaitForSeconds(0.5f);
                SceneManager.LoadScene("Game");
                break;
            case 4:
                ComicsImage[1].SetActive(true);
                ComicsImage[1].transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1f);
                ComicsImage[1].transform.GetChild(1).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ComicsImage[1].transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ComicsImage[1].transform.GetChild(1).GetChild(1).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.9f);
                ComicsImage[1].transform.GetChild(1).GetChild(1).GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(2f);
                transform.GetChild(3).GetComponent<Animator>().Play("Close_Close_Panel");
                yield return new WaitForSeconds(0.5f);
                SceneManager.LoadScene("Game");
                break;
            case 9:
                ComicsImage[2].SetActive(true);
                ComicsImage[2].transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1f);
                ComicsImage[2].transform.GetChild(1).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ComicsImage[2].transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ComicsImage[2].transform.GetChild(1).GetChild(1).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ComicsImage[2].transform.GetChild(1).GetChild(2).gameObject.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ComicsImage[2].transform.GetChild(1).GetChild(3).gameObject.SetActive(true);
                yield return new WaitForSeconds(1f);
                ComicsImage[2].transform.GetChild(1).GetChild(3).GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(2f);
                transform.GetChild(3).GetComponent<Animator>().Play("Close_Close_Panel");
                yield return new WaitForSeconds(0.5f);
                SceneManager.LoadScene("Game");
                break;
        }
    }

}

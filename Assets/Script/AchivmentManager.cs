﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchivmentManager : MonoBehaviour {

    public GameObject[] Achivments;

    public Sprite TitleAchiOpen
        , active;

    void Start() {
        CheckAchivment();
    }

    void CheckAchivment() {
        for (int i = 0; i < GlobalData.Achivment.Length; i++) {
            if (GlobalData.Achivment[i] == 1)
            {
                Achivments[i].GetComponent<Image>().sprite = TitleAchiOpen;
                Achivments[i].transform.GetChild(0).GetComponent<Image>().sprite = active;
                Achivments[i].transform.GetChild(2).gameObject.SetActive(false);
            }
        }
    }
	
}

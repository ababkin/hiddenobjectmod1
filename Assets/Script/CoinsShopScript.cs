﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinsShopScript : MonoBehaviour {

    public GameObject _description;

    public void Buy(int _case) {
        switch (_case) {
            case 1:
                if (GlobalData.Money >= 75)
                {
                    transform.parent.GetComponent<AudioSource>().Play();
                    GlobalData.HelpScore += 5;
                    GlobalData.Money -= 75;
                    ShowMessage("You bought 5 tips");
                    PlayerPrefs.SetInt("Money", GlobalData.Money);
                    PlayerPrefs.SetInt("Hint", GlobalData.HelpScore);
                }
                else transform.SetAsFirstSibling();
                break;
            case 2:
                if (GlobalData.Money >= 150)
                {
                    transform.parent.GetComponent<AudioSource>().Play();
                    GlobalData.HelpScore += 12;
                    GlobalData.Money -= 150;
                    ShowMessage("You bought 12 tips");
                    PlayerPrefs.SetInt("Money", GlobalData.Money);
                    PlayerPrefs.SetInt("Hint", GlobalData.HelpScore);
                }
                else transform.SetAsFirstSibling();
                break;
            case 3:
                if (GlobalData.Money >= 200)
                {
                    transform.parent.GetComponent<AudioSource>().Play();
                    GlobalData.HelpScore +=20;
                    GlobalData.Money -= 200;
                    ShowMessage("You bought 20 tips");
                    PlayerPrefs.SetInt("Money", GlobalData.Money);
                    PlayerPrefs.SetInt("Hint", GlobalData.HelpScore);
                }
                else transform.SetAsFirstSibling();
                break;
            case 4:
                if (GlobalData.Money >= 400)
                {
                    transform.parent.GetComponent<AudioSource>().Play();
                    GlobalData.HelpScore += 50;
                    GlobalData.Money -= 400;
                    ShowMessage("You bought 50 tips");
                    PlayerPrefs.SetInt("Money", GlobalData.Money);
                    PlayerPrefs.SetInt("Hint", GlobalData.HelpScore);
                }
                else transform.SetAsFirstSibling();
                break;
        }
    }

    public void ShowMessage(string _message) {
        _description.SetActive(true);
        _description.transform.GetChild(1).GetComponent<Text>().text = _message;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using AppodealAds.Unity.Api;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public int CountGenerate;
    public Transform _allObject;
    public GameObject _object,
        Pause_Panel,
        Complite_Panel,
        GameOver_Panel,
        Game_Interface,
        Dialog_Panel,
        ShowAchivment,
        Shop;
    public Image[] _questImage;
    public Text[] _titleNumberComplite;
    public Text TitleTimer, TitleMoney, CountHelp;
    public float _timerGame, _startTimerGame;
    public int numberStage, roundMoney;
    public int[] _stageComplite;
    public Animator _closePanelAnimator;
    public bool noHints = true;
    public float _timerAchivment,
        _timerCallHelp;

    private int i = 0;
    

    [Space(20)]
    public Sprite[] _questSprite;
    public Sprite _Ready,
        _go,
        _good,
        _excellent;
    public List<GameObject> Stage1Obj = new List<GameObject>(),
        Stage2Obj = new List<GameObject>(),
        Stage3Obj = new List<GameObject>();
    private float timer,_penaltyTimer;
    public GameObject _goodMessage,_badMessage;
    private int tap;

    private void Start()
    {
        Quest();
        StartCoroutine(AllCorontine("Init"));
        TimerGameLevel(GlobalData.NumberLevel);
        Debug.Log(GlobalData.NumberLevel);
        if (GlobalData.NumberLevel == 4 && GlobalData.Achivment[3]==0) {
            GlobalData.Achivment[3] = 1;
            GlobalData.Money += 150;
            ShowAchivment.SetActive(true);
            PlayerPrefs.SetInt("Achivment3", GlobalData.Achivment[3]);
            PlayerPrefs.SetInt("Money", GlobalData.Money);
        }
        if (GlobalData.NumberLevel == 9 && GlobalData.Achivment[4] == 0)
        {
            GlobalData.Achivment[4] = 1;
            GlobalData.Money += 1000;
            ShowAchivment.SetActive(true);
            PlayerPrefs.SetInt("Achivment4", GlobalData.Achivment[4]);
            PlayerPrefs.SetInt("Money", GlobalData.Money);
        }
    }
    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), new Vector3(0, 0, 20));
            if (hit.collider != null)
            {
                FindObj(hit, numberStage);
                _timerCallHelp = 10;
            }
           else {
                PenaltyTime();
                Debug.Log("There is no object!");
            }
        }
    }
    private void Awake() {
        _closePanelAnimator.gameObject.SetActive(true);
    }
    private void Update()
    {
        _titleNumberComplite[0].text = _stageComplite[0].ToString("0");
        _titleNumberComplite[1].text = _stageComplite[1].ToString("0");
        _titleNumberComplite[2].text = _stageComplite[2].ToString("0");
        GameTimer();
        CheckDubleSprite();
        ColliderDetect();
        if (timer > 0) timer -= Time.deltaTime;
        else timer = 0;
        if (_penaltyTimer > 0) _penaltyTimer -= Time.deltaTime;
        CallHalpButton();
    }
    private void TimerGameLevel(int _numberLevel) {
        switch (_numberLevel) {
            case 0:
                _startTimerGame = 30;
                break;
            case 1:
                _startTimerGame = 30;
                break;
            case 2:
                _startTimerGame = 40;
                break;
            case 3:
                _startTimerGame = 40;
                break;
            case 4:
                _startTimerGame = 30;
                break;
            case 5:
                _startTimerGame = 30;
                break;
            case 6:
                _startTimerGame = 30;
                break;
            case 7:
                _startTimerGame = 30;
                break;
            case 8:
                _startTimerGame = 30;
                break;
            case 9:
                _startTimerGame = 30;
                break;
        }
        _timerGame = _startTimerGame;
    }
    private void PenaltyTime() {
        if (!GameOver_Panel.activeInHierarchy && !Complite_Panel.activeInHierarchy && !Pause_Panel.activeInHierarchy && !Shop.activeInHierarchy)
        {
            tap += 1;
            _penaltyTimer = 1;
            if (_penaltyTimer > 0)
            {
                if (tap == 5)
                {
                    _badMessage.SetActive(true);
                    TitleTimer.transform.GetChild(0).gameObject.SetActive(true);
                    StartCoroutine(GlobalData.SetActive(_badMessage, 1.15f, false));
                    StartCoroutine(GlobalData.SetActive(TitleTimer.transform.GetChild(0).gameObject, 1.15f, false));
                    _timerGame -= 10;
                    tap = 0;
                    Debug.Log("Penalty");
                }
            }
        }
    }
    private GameObject ObjectGenerate(int _count)
    {
        if (_count > 0)
        {
            object[] _allObject = Resources.LoadAll("Stickers/stikers");
            int ObjRandom = Random.Range(1, 33);
            GameObject _obj = Instantiate(_object);
            _obj.GetComponent<SpriteRenderer>().sprite = (Sprite)_allObject[ObjRandom];
            _obj.GetComponent<Collider2D>().enabled = false;
            _obj.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            _obj.transform.position = new Vector2(Random.Range(-7f,7f), Random.Range(-3.5f,2.5f));
            _obj.transform.parent = this._allObject;
            _count -= 1;
            ObjectGenerate(_count);
            return _obj;
        }
        else {
            return null;
        }
    }
    private IEnumerator AllCorontine(string _case)
    {
        switch (_case)
        {
            case "Init":
                Dialog_Panel.SetActive(true);
                yield return new WaitForSeconds(0.1f);
                Dialog_Panel.transform.GetChild(0).GetComponent<Image>().sprite = _Ready;
                Dialog_Panel.transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1.4f);
                Dialog_Panel.transform.GetChild(0).gameObject.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                Dialog_Panel.transform.GetChild(0).GetComponent<Image>().sprite = _go;
                Dialog_Panel.transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1.4f);
                Dialog_Panel.GetComponent<Animator>().Play("Close_Dialog");
                yield return new WaitForSeconds(0.5f);
                Dialog_Panel.SetActive(false);
                Game_Interface.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                ObjectGenerate(CountGenerate);
                #region QuestObjectGenerate
                switch (GlobalData.NumberLevel) {
                    case 0:
                        QuestObject(3, 3, 3);
                        break;
                    case 1:
                        QuestObject(4, 4, 4);
                        break;
                    case 2:
                        QuestObject(5, 5, 5);
                        break;
                    case 3:
                        QuestObject(7, 7, 7);
                        break;
                    case 4:
                        QuestObject(7, 7, 7);
                        break;
                    case 5:
                        QuestObject(7, 7, 7);
                        break;
                    case 6:
                        QuestObject(Random.Range(5, 10), Random.Range(5, 10), Random.Range(5, 10));
                        break;
                    case 7:
                        QuestObject(Random.Range(5, 10), Random.Range(5, 10), Random.Range(5, 10));
                        break;
                    case 8:
                        QuestObject(Random.Range(5, 10), Random.Range(5, 10), Random.Range(5, 10));
                        break;
                    case 9:
                        QuestObject(Random.Range(5, 10), Random.Range(5, 10), Random.Range(5, 10));
                        break;
                }
                #endregion
                _questImage[0].gameObject.SetActive(true);
                ColliderDetect();
                break;
            case "Help":

                break;
            case "Level_Complite":
                Complite_Panel.SetActive(true);
                break;
            case "Continue":
                _closePanelAnimator.Play("Close_Close_Panel");
                yield return new WaitForSeconds(0.8f);
                LevelComplite();
                break;
            case "Close_Options":
                Pause_Panel.GetComponent<Animator>().Play("Pause_Panel_Close");
                yield return new WaitForSeconds(0.5f);
                Pause_Panel.SetActive(false);
                break;
            case "Detect":
                ColliderDetect();
                break;
            case "CloseGoodMessage":
                yield return new WaitForSeconds(1.15f);
                _goodMessage.SetActive(false);
                break;
        }
    }
    private void Quest()
    {
        for (int i = 0; i < _questImage.Length; i++)
        {
            int numberImage = Random.Range(0, 4);
            _questImage[i].sprite = _questSprite[numberImage] as Sprite;
        }
    }
    private void CheckDubleSprite() {
        if (_questImage[2].sprite == _questImage[1].sprite || _questImage[2].sprite == _questImage[0].sprite) {
            int numberImage = Random.Range(0, 4);
            _questImage[2].sprite = _questSprite[numberImage] as Sprite;
        }
        if (_questImage[1].sprite == _questImage[0].sprite) {
            int numberImage = Random.Range(0, 4);
            _questImage[1].sprite = _questSprite[numberImage] as Sprite;
        }
    }
    private void ColliderDetect() {
        switch (numberStage)
        {
            case 1:
                Stage2Obj.ForEach(delegate (GameObject st)
                {
                    if (st != null) st.GetComponent<Collider2D>().enabled = false;
                });
                Stage3Obj.ForEach(delegate (GameObject st)
                {
                    if (st != null) st.GetComponent<Collider2D>().enabled = false;
                });
                break;
            case 2:
                Stage2Obj.ForEach(delegate (GameObject st)
                {
                    if (st != null) st.GetComponent<Collider2D>().enabled = true;
                });
                Stage3Obj.ForEach(delegate (GameObject st)
                {
                    if (st != null) st.GetComponent<Collider2D>().enabled = false;
                });
                break;
            case 3:
                Stage3Obj.ForEach(delegate (GameObject st)
                {
                    if (st != null) st.GetComponent<Collider2D>().enabled = true;
                });
                break;
        }
    }
    private void FindObj(RaycastHit2D _hit, int _stage)
    {
        if (_stage == 1)
        {
            if (_hit.collider.tag == "Stage 1" && !_hit.collider.gameObject.GetComponent<ObjScript>()._findMe)
            {
                _hit.collider.GetComponent<ObjScript>()._findMe = true;
                StartCoroutine(GlobalData.SetActive(_hit.collider.gameObject, 0.8f, false));
                _stageComplite[0] -= 1;
                GlobalData.Money += 5;
                roundMoney += 5;
                Debug.Log("Object");
                if (timer > 0)
                {
                    _goodMessage.SetActive(true);
                    StartCoroutine(AllCorontine("CloseGoodMessage"));
                    timer = 1;
                }
                else timer = 1;
                if (_stageComplite[0] == 0)
                {
                    _questImage[0].color = Color.gray;
                    numberStage += 1;
                    _questImage[1].gameObject.SetActive(true);
                    StartCoroutine(AllCorontine("Detect"));
                }
            }
        }
        else if (_stage == 2)
        {
            if (_hit.collider.tag == "Stage 2" && !_hit.collider.gameObject.GetComponent<ObjScript>()._findMe)
            {
                _hit.collider.gameObject.GetComponent<ObjScript>()._findMe = true;
                // Destroy(_hit.collider.gameObject, 2);
                StartCoroutine(GlobalData.SetActive(_hit.collider.gameObject, 0.8f, false));
                _stageComplite[1] -= 1;
                GlobalData.Money += 5;
                roundMoney += 5;
                if (timer > 0)
                {
                    _goodMessage.SetActive(true);
                    StartCoroutine(AllCorontine("CloseGoodMessage"));
                    timer = 1;
                }
                else timer = 1;
                if (_stageComplite[1] == 0)
                {
                    _questImage[1].color = Color.gray;
                    numberStage += 1;
                    _questImage[2].gameObject.SetActive(true);
                    StartCoroutine(AllCorontine("Detect"));
                }
            }
        }
        else if (_stage == 3)
        {
            if (_hit.collider.tag == "Stage 3" && !_hit.collider.gameObject.GetComponent<ObjScript>()._findMe)
            {
                _hit.collider.gameObject.GetComponent<ObjScript>()._findMe = true;
                // Destroy(_hit.collider.gameObject, 2);
               StartCoroutine(GlobalData.SetActive(_hit.collider.gameObject, 0.8f, false));
                _stageComplite[2] -= 1;
                GlobalData.Money += 5;
                roundMoney += 5;
                if (timer > 0)
                {
                    _goodMessage.SetActive(true);
                    StartCoroutine(AllCorontine("CloseGoodMessage"));
                    timer = 1;
                }
                else timer = 1;
                if (_stageComplite[2] == 0)
                {
                    _questImage[2].color = Color.gray;
                    StartCoroutine(AllCorontine("Level_Complite"));
                    StartCoroutine(AllCorontine("Detect"));
                }
            }
        }
        GetComponent<AudioSource>().Play();
        //return;
    }
    public void LevelComplite()
    {
        SceneManager.LoadScene("Game");
        Debug.Log("Level Complite!");
    }
    public void Help()
    {
        if (GlobalData.HelpScore >= 1)
        {
            if (numberStage == 1)
            {
                for (int i = 0; i < Stage1Obj.Count; i++)
                {
                    if (Stage1Obj[i].activeSelf) {
                        if (!Stage1Obj[i].GetComponent<ObjScript>()._help) {
                            Stage1Obj[i].GetComponent<ObjScript>()._help = true;
                            Stage1Obj[i].GetComponent<ObjScript>().LightOn();
                            StartCoroutine(GlobalData.SetActive(Stage1Obj[i].transform.GetChild(0).gameObject, 1.5f, false));
                            
                        }
                    }
                }
            }
            else if (numberStage == 2)
            {
                for (int i = 0; i < Stage2Obj.Count; i++)
                {
                    if (Stage2Obj[i].activeSelf)
                    {
                        if (!Stage2Obj[i].GetComponent<ObjScript>()._help)
                        {
                            Stage2Obj[i].GetComponent<ObjScript>()._help = true;
                            Stage2Obj[i].GetComponent<ObjScript>().LightOn();
                            StartCoroutine(GlobalData.SetActive(Stage2Obj[i].transform.GetChild(0).gameObject, 1.5f, false));
                        }
                    }
                }
            }
            else if (numberStage == 3)
            {
                for (int i = 0; i < Stage3Obj.Count; i++)
                {
                    if (Stage3Obj[i].activeSelf)
                    {
                        if (!Stage3Obj[i].GetComponent<ObjScript>()._help)
                        {
                            Stage3Obj[i].GetComponent<ObjScript>()._help = true;
                            Stage3Obj[i].GetComponent<ObjScript>().LightOn();
                            StartCoroutine(GlobalData.SetActive(Stage3Obj[i].transform.GetChild(0).gameObject, 1.5f, false));
                        }
                    }
                }
            }
            if(GlobalData.Achivment[0] == 0) noHints = false;
            GlobalData.HelpScore -= 1;
            PlayerPrefs.SetInt("Hint", GlobalData.HelpScore);
        }
        else if (GlobalData.HelpScore == 0) {
            Shop.SetActive(true);
            Shop.transform.GetChild(0).SetAsFirstSibling();
        }
        _timerCallHelp = 0;
    }
    private void GameTimer()
    {
        TitleMoney.text = GlobalData.Money.ToString();
        if (_timerGame > 0 && Game_Interface.activeInHierarchy && !Complite_Panel.activeInHierarchy && !Pause_Panel.activeInHierarchy && !Shop.activeInHierarchy)
        {
            if (_timerAchivment < 15) _timerAchivment += Time.deltaTime;
            _timerGame -= Time.deltaTime;
            TitleTimer.text = "0:" + _timerGame.ToString("00");
            CountHelp.text = GlobalData.HelpScore.ToString();
        }
        else if (_timerGame <= 0)
        {  
            while (i == 0) {
                GameOver();
                TitleTimer.text = "0:00";
                i += 1;
            }
        }
    }
    private void GameOver() {
        GameOver_Panel.SetActive(true);
        if (GlobalData.Money >= 50) GameOver_Panel.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
        else GameOver_Panel.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
        Appodeal.show(Appodeal.INTERSTITIAL);
        Debug.Log("GameOver");
    }
    private GameObject QuestObject(int count_1, int count_2, int count_3)
    {
        if (count_1 > 0)
        {
            GameObject _questObj = Instantiate(_object);
            _questObj.GetComponent<SpriteRenderer>().sprite = _questImage[0].sprite;
            _questObj.tag = "Stage 1";
            _stageComplite[0] += 1;
            _questObj.GetComponent<SpriteRenderer>().sortingOrder = 3;
            _questObj.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            _questObj.transform.position = new Vector2(Random.Range(-7f, 7f), Random.Range(-3.5f, 2.5f)); ;
            _questObj.transform.parent = this._allObject;
            count_1 -= 1;
            Stage1Obj.Add(_questObj);
            QuestObject(count_1, count_2, count_3);
            return _questObj;
        }
        else if (count_1 <= 0 && count_2 > 0)
        {
            GameObject _questObj = Instantiate(_object);
            _questObj.GetComponent<SpriteRenderer>().sprite = _questImage[1].sprite;
            _questObj.tag = "Stage 2";
            _stageComplite[1] += 1;
            _questObj.GetComponent<SpriteRenderer>().sortingOrder = 2;
            _questObj.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            _questObj.transform.position = new Vector2(Random.Range(-Screen.width * 0.004f, Screen.width * 0.004f), Random.Range(-Screen.height * 0.00345f, Screen.height * 0.00245f));
            _questObj.transform.parent = this._allObject;
            count_2 -= 1;
            Stage2Obj.Add(_questObj);
            QuestObject(count_1, count_2, count_3);
            return _questObj;
        }
        else if (count_2 <= 0 && count_3 > 0)
        {
            GameObject _questObj = Instantiate(_object);
            _questObj.GetComponent<SpriteRenderer>().sprite = _questImage[2].sprite;
            _questObj.tag = "Stage 3";
            _stageComplite[2] += 1;
            _questObj.GetComponent<SpriteRenderer>().sortingOrder = 1;
            _questObj.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            _questObj.transform.position = new Vector2(Random.Range(-Screen.width * 0.004f, Screen.width * 0.004f), Random.Range(-Screen.height * 0.00345f, Screen.height * 0.00245f));
            _questObj.transform.parent = this._allObject;
            count_3 -= 1;
            Stage3Obj.Add(_questObj);
            QuestObject(count_1, count_2, count_3);
            return _questObj;
        }
        return null;

    }
    public void PlusTime() {
        if (GlobalData.Money >= 180)
        {
            _timerGame += 15;
            GameOver_Panel.SetActive(false);
            GlobalData.Money -= 180;
            i = 0;
        }
        else {
            Debug.Log("NoMoney");
        }
    }
    public void Buttons(string name) {
        switch (name) {
            case "Close_Pause_Panel":
                StartCoroutine(AllCorontine("Close_Options"));
                break;
        }
    }
    private void CallHalpButton() {
        if (GlobalData.HelpScore.Equals(0) || !Game_Interface.activeInHierarchy) return;

        if (_timerCallHelp <= 0)
        {
            Game_Interface.transform.FindChild("HelpButton").GetComponent<Animator>().Play("HelpButton");
            _timerCallHelp = 10;
        }
        else {
            _timerCallHelp -= Time.deltaTime;
        }
    }


}

﻿using UnityEngine;
using System.Collections;

public static class GlobalData {

    public static int HelpScore;
    public static int NumberLevel;
    public static int[] UnlockLevel = new int[10]
        , Achivment = new int[5];
    public static int Money, Stars, Keys;

    public static bool SoundEnabled = true;


    //Purchase Data
    public static int NoADS = 0;



    /// <summary>
    /// Enabled and Disabled GameObject in time
    /// </summary>
    /// <param name="_object"></param>
    /// <param name="_timer"></param>
    /// <param name="active"></param>
    /// <returns></returns>
    public static IEnumerator SetActive(GameObject _object, float _timer, bool active) {
        if (active)
        {
            yield return new WaitForSeconds(_timer);
            _object.SetActive(true);
        }
        else {
            yield return new WaitForSeconds(_timer);
            _object.SetActive(false);
        }
        Debug.Log("Object: " + _object.name + " - " + active.ToString());
    }
}

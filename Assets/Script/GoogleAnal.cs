﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GoogleAnal : MonoBehaviour {

    public GoogleAnalyticsV3 Gav3;

    void Start() {
        DontDestroyOnLoad(gameObject);
        Gav3.LogScreen(SceneManager.GetActiveScene().name);
    }
}

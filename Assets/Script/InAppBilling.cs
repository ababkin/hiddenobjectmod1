﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class InAppBilling : MonoBehaviour , IStoreListener {

    public static InAppBilling instance;
    public string[] _product;
    public Text[] _priceTitle;
    private IStoreController controller;
    private IExtensionProvider extensions;
    public CoinsShopScript _coinShop;

    private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

    void Awake() {
        instance = this;
        DontDestroyOnLoad(transform.gameObject);
    }

    void Start() {
        InitPurchase();
        StartCoroutine(InitPrice());
    }

  //  void Update() {
        
  //  }

    /// <summary>
    /// Добавление покупок
    /// </summary>
    public void InitPurchase() {
        if (IsInitialized()) return;

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(_product[0], ProductType.Consumable);
        builder.AddProduct(_product[1], ProductType.Consumable);
        builder.AddProduct(_product[2], ProductType.Consumable);
        builder.AddProduct(_product[3], ProductType.Consumable);
        builder.AddProduct(_product[4], ProductType.NonConsumable);
        builder.AddProduct(_product[5], ProductType.NonConsumable);
        builder.AddProduct(_product[6], ProductType.Consumable);
        builder.AddProduct(_product[7], ProductType.Consumable);
        builder.AddProduct(_product[8], ProductType.Consumable);
        UnityPurchasing.Initialize(instance, builder);
    }

    private bool IsInitialized()
    {
        return controller != null && extensions != null;
    }

    /// <summary>
    /// Инитиализация
    /// </summary>
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
        this.controller = controller;
        this.extensions = extensions;
    }

    /// <summary>
    /// Если инитиализация неудачна
    /// </summary>
    public void OnInitializeFailed(InitializationFailureReason error) {
        Debug.Log(error);
    }

    /// <summary>
    /// Если покупка выполнена успешно!
    /// </summary>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        for (int i = 0; i < _product.Length; i++) {
            if (String.Equals(e.purchasedProduct.definition.id, _product[i], StringComparison.Ordinal)) {
                GetComponent<AudioSource>().Play();
                switch (i) {
                    case 0:
                        GlobalData.Money += 100;
                        PlayerPrefs.SetInt("Money", GlobalData.Money);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought 100 Cold");
                        break;
                    case 1:
                        GlobalData.Money += 250;
                        PlayerPrefs.SetInt("Money", GlobalData.Money);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought 250 Cold");
                        break;
                    case 2:
                        GlobalData.Money += 750;
                        PlayerPrefs.SetInt("Money", GlobalData.Money);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought 750 Cold");
                        break;
                    case 3:
                        GlobalData.Money += 2500;
                        PlayerPrefs.SetInt("Money", GlobalData.Money);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought 2500 Cold");
                        break;
                    case 4:
                        GlobalData.NoADS = 1;
                        PlayerPrefs.SetInt("NoADS", GlobalData.NoADS);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought NoAds");
                        break;
                    case 5:
                        GlobalData.Money += 1000;
                        PlayerPrefs.SetInt("Money", GlobalData.Money);
                        GlobalData.NoADS = 1;
                        PlayerPrefs.SetInt("NoADS", GlobalData.NoADS);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought NoAds + 1000 Cold");
                        break;
                    case 6:
                        GlobalData.Keys += 1;
                        PlayerPrefs.SetInt("Keys", GlobalData.Keys);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought 1 key");
                        break;
                    case 7:
                        GlobalData.Keys += 3;
                        PlayerPrefs.SetInt("Keys", GlobalData.Keys);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought 3 keys");
                        break;
                    case 8:
                        GlobalData.Keys += 9;
                        PlayerPrefs.SetInt("Keys", GlobalData.Keys);
                        Debug.Log("Buy:" + _product[i]);
                        _coinShop.ShowMessage("You bought 9 keys");
                        break;
                }
            } 
        }
        //Debug.Log(e.purchasedProduct.availableToPurchase);
        return PurchaseProcessingResult.Complete;
    }

    /// <summary>
    /// Если покупка не выполнена!
    /// </summary>
    public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
        Debug.LogError(i + " " + p);
    }

    public void OnPurchaseClicked(string productID)
    {
        controller.InitiatePurchase(productID);
    }

    IEnumerator InitPrice() {
        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < _product.Length; i++) {
            if (IsInitialized()) _priceTitle[i].text = controller.products.WithID(_product[i]).metadata.localizedPriceString.ToString();
            else _priceTitle[i].text = "No Network!";
        }
    }

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = controller.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));                                                                                        // asynchronously.
                controller.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void ByProduct(int _id)
    {
        BuyProductID(_product[_id]);
    }
}

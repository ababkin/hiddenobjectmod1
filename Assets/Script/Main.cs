﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main : MonoBehaviour {

    public GameObject _closePanel,
        SoundButton;

    [Space(10)]
    public Animator _closePanelAnim;
    public Sprite _soundOn,
        _soundOff;

    public AudioClip[] _levelMusic;
    public MyAppodeal ads;
    private AudioSource music;

    void Awake() {
        if (_closePanel != null) _closePanel.SetActive(true);
    }

    void Start() {
        music = GameObject.Find("Music").GetComponent<AudioSource>();
        if (SceneManager.GetActiveScene().name == "Main" || SceneManager.GetActiveScene().name == "Select_Level")
        {
            music.clip = _levelMusic[0];
        }
        else {
            music.clip = _levelMusic[1];
        }
        if (PlayerPrefs.HasKey("Money")) GlobalData.Money = PlayerPrefs.GetInt("Money");
        if (PlayerPrefs.HasKey("Hint")) GlobalData.HelpScore = PlayerPrefs.GetInt("Hint");
        else GlobalData.HelpScore = 3;
        if (PlayerPrefs.HasKey("NoADS")) GlobalData.NoADS = PlayerPrefs.GetInt("NoADS");
        if (PlayerPrefs.HasKey("Keys")) GlobalData.Keys = PlayerPrefs.GetInt("Keys");
        if (PlayerPrefs.HasKey("Achivment0")) GlobalData.Achivment[0] = PlayerPrefs.GetInt("Achivment0");
        if (PlayerPrefs.HasKey("Achivment1")) GlobalData.Achivment[1] = PlayerPrefs.GetInt("Achivment1");
        if (PlayerPrefs.HasKey("Achivment2")) GlobalData.Achivment[2] = PlayerPrefs.GetInt("Achivment2");
        if (PlayerPrefs.HasKey("Achivment3")) GlobalData.Achivment[3] = PlayerPrefs.GetInt("Achivment3");
        if (PlayerPrefs.HasKey("Achivment4")) GlobalData.Achivment[4] = PlayerPrefs.GetInt("Achivment4");
    }

    void Update() {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.D)) {
            PlayerPrefs.DeleteAll();
            Debug.Log("DeleteAllData");
        }
#endif
    }

    public void Buttons(string _name) {
        switch (_name) {
            case "comics":
                _closePanelAnim.Play("Close_Close_Panel");
                StartCoroutine(AllCorontine("comics"));
                break;
            case "Game":
                _closePanelAnim.Play("Close_Close_Panel");
                StartCoroutine(AllCorontine("Game"));
                break;
            case"Main":
                _closePanelAnim.Play("Close_Close_Panel");
                StartCoroutine(AllCorontine("Main"));
                break;
            case "Continue":
                _closePanelAnim.Play("Close_Close_Panel");
                StartCoroutine(AllCorontine("Continue"));
                break;
            case "SoundOn":
                if (SoundButton != null) {
                    if (GlobalData.SoundEnabled)
                    {
                        SoundButton.GetComponent<Image>().sprite = _soundOff;
                        GlobalData.SoundEnabled = false;
                    }
                    else if (!GlobalData.SoundEnabled) {
                        SoundButton.GetComponent<Image>().sprite = _soundOn;
                        GlobalData.SoundEnabled = true;
                    }
                }
                break;
        }
    }

    IEnumerator AllCorontine(string name) {
        switch (name)
        {
            case "Game":
                PlayerPrefs.SetInt("Money", GlobalData.Money);
                Debug.Log("Save");
                yield return new WaitForSeconds(0.5f);
                ads.ShowADS("FullScreen");
                SceneManager.LoadScene("Game");
                break;
            case "comics":
                PlayerPrefs.SetInt("Money", GlobalData.Money);
                Debug.Log("Save");
                yield return new WaitForSeconds(0.5f);
                SceneManager.LoadScene("Comics");
                break;
            case "Main":
                PlayerPrefs.SetInt("Money", GlobalData.Money);
                Debug.Log("Save");
                yield return new WaitForSeconds(0.5f);
                SceneManager.LoadScene("Main");
                break;
            case "Continue":
                PlayerPrefs.SetInt("Money", GlobalData.Money);
                Debug.Log("Save");
                yield return new WaitForSeconds(0.5f);
                SceneManager.LoadScene("Select_Level");
                break;
        }
    }
}

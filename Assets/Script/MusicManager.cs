﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public static bool muze;
    AudioSource source;

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    void Start() {
        source = GetComponent<AudioSource>();
        MusicInit();
    }

    void Update() {
        if (!source.isPlaying)
            source.Play();
        if (!GlobalData.SoundEnabled) {
            source.Stop();
        }
    }

    void MusicInit()
    {
        if (GlobalData.SoundEnabled)
        {
            if (!muze)
            {
                source.Play();
                muze = true;
            }
            else Destroy(source.gameObject);
        }
    }
}

﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MyADS : MonoBehaviour {

    public GameObject _banner;
    public string[] wwwIcon;
    public Image[] sprite;
    public string[] _prise;
    int _currentDay;
    

    void Start()
    {
        StartCoroutine(LoadImageUrl());
        if (PlayerPrefs.HasKey("day")) _currentDay = PlayerPrefs.GetInt("day");
    }

    IEnumerator LoadImageUrl() {
        for (int i = 0; i < wwwIcon.Length; i++)
        {
            WWW www = new WWW(wwwIcon[i]);
            yield return www;
            if (i != 4)
            {
                Sprite ImageIc1 = Sprite.Create(www.texture, new Rect(0, 0, 512, 512), new Vector2(0.1f, 0.1f));
                sprite[i].sprite = ImageIc1;
            }
            else {
                Sprite ImageIc1 = Sprite.Create(www.texture, new Rect(0, 0, 960, 540), new Vector2(0.1f, 0.1f));
                sprite[i].sprite = ImageIc1;
            }
        }
        yield return new WaitForSeconds(0.2f);
        ShowDayBanner();
    }

    void ShowDayBanner() {
        if (DateTime.Now.Day != _currentDay) {
            _banner.SetActive(true);
            _currentDay = DateTime.Now.Day;
            PlayerPrefs.SetInt("day", _currentDay);
        } 
    }

    public void OpenUrl(string _url)
    {
        Application.OpenURL(_url);
    }
}

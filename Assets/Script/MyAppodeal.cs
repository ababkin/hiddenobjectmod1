﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

public class MyAppodeal : MonoBehaviour, IInterstitialAdListener, INonSkippableVideoAdListener, IRewardedVideoAdListener
{
    public string AppodealKey;
    public level_complite _complite;

    void Start()
    {
        Appodeal.setInterstitialCallbacks(this);
        Appodeal.setNonSkippableVideoCallbacks(this);
        Appodeal.setRewardedVideoCallbacks(this);
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(AppodealKey, Appodeal.BANNER_BOTTOM | Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO);
        if (GlobalData.NoADS == 0) ShowADS("ShowBanner");
    }

    public void ShowADS(string _type)
    {
        if (GlobalData.NoADS == 0)
        {
            switch (_type)
            {
                case "FullScreen":
                    Appodeal.show(Appodeal.INTERSTITIAL);
                    Debug.Log("fullscreen");
                    break;
                case "RewardVideo":
                    Appodeal.show(Appodeal.REWARDED_VIDEO);
                    Debug.Log("video");
                    break;
                case "ShowBanner":
                    Appodeal.show(Appodeal.BANNER_BOTTOM);
                    break;
                case "HideBanner":
                    Appodeal.hide(Appodeal.BANNER);
                    break;
            }
        }
    }

    public void onInterstitialLoaded()
    {
        Debug.Log("Interstitial loaded");
    }
    public void onInterstitialFailedToLoad()
    {
        Debug.Log("Interstitial failed");
        StopCoroutine("tryShowAd");
    }
    public void onInterstitialShown()
    {
        Debug.Log("Interstitial opened");
    }
    public void onInterstitialClosed()
    {
        Debug.Log("Interstitial closed");

    }
    public void onInterstitialClicked()
    {
        Debug.Log("Interstitial clicked");
    }

    //------------------
    public void onNonSkippableVideoLoaded()
    {
        Debug.Log("NonSkipableVideoLoaded");
    }
    public void onNonSkippableVideoFailedToLoad()
    {
        Debug.Log("NonSkipableVideoFailedToLoaded");
    }
    public void onNonSkippableVideoShown()
    {
        Debug.Log("NonSkipableVideoShown");
        //dontShow = true;
    }
    public void onNonSkippableVideoFinished()
    {
        Debug.Log("NonSkipableVideoFinished");
    }
    public void onNonSkippableVideoClosed()
    {
        Debug.Log("NonSkipableVideoClosed");
    }

    //-----------------

    public void onRewardedVideoLoaded()
    {
        
    }

    public void onRewardedVideoFailedToLoad()
    {
        throw new NotImplementedException();
    }

    public void onRewardedVideoShown()
    {
        throw new NotImplementedException();
    }

    public void onRewardedVideoFinished(int amount, string name)
    {
        throw new NotImplementedException();
    }

    public void onRewardedVideoClosed()
    {
        if (_complite != null)
        {
            _complite.RewardedVideo(false);
        }
    }
}

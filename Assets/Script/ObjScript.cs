﻿using UnityEngine;
using System.Collections;

public class ObjScript : MonoBehaviour
{

    public bool _findMe, _help, _count, _helpCount;
    private Animator anim;
    public GameObject _plus5;
    private float _timer = 0.1f;
    public GameObject _sunShine;

    void Start()
    {
        _count = true;
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        anim.SetBool("FindObj", _findMe);
        anim.SetBool("Help", _help);

        if(_sunShine.activeInHierarchy) _sunShine.transform.Rotate(0, 0, 2);

        if (_help && _timer > 0)
        {
            _timer -= Time.deltaTime;
        }
        else if (_timer <= 0)
        {
            _help = false;
            _timer = 0.1f;
        }

        if (_findMe)
        {
            transform.Rotate(0, 0, 15);
            if (_count)
            {
                GameObject _obj = (GameObject)Instantiate(_plus5, transform.position, Quaternion.identity);
                _count = false;
            }
        }
    }
    public void LightOn()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }
}

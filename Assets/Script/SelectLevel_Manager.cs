﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectLevel_Manager : MonoBehaviour
{

    public GameObject[] Levels;
    public Text TitleMoney, TitleStars, TitleKeys;
    public GameObject _noMoney;
    public GameObject PricePopap, _theEnd, Shop;
    public MyAppodeal ads;
    int preNumer ,_nL;

    [Space(20)]
    public Sprite[] _numbers;
    public Sprite _lockImage, _openImage, _star, _nullStar;
    Main main;

    void Start()
    {
        ads.ShowADS("FullScreen");
        main = GetComponent<Main>();
        LoadData();
        CheckLevels();
        if (GlobalData.UnlockLevel[9] != 0 && GlobalData.UnlockLevel[9] != 4) _theEnd.SetActive(true);
    }

    void Update() {
        UpdateMoney();
    }

    void UpdateMoney()
    {
        TitleMoney.text = GlobalData.Money.ToString();
        TitleStars.text = GlobalData.Stars.ToString() + "/30";
        TitleKeys.text = GlobalData.Keys.ToString("0");
    }

    void LoadData()
    {
        if (PlayerPrefs.HasKey("UnlockLevel_0")) GlobalData.UnlockLevel[0] = PlayerPrefs.GetInt("UnlockLevel_0");
        else GlobalData.UnlockLevel[0] = 4;
        if (PlayerPrefs.HasKey("UnlockLevel_1")) GlobalData.UnlockLevel[1] = PlayerPrefs.GetInt("UnlockLevel_1");
        if (PlayerPrefs.HasKey("UnlockLevel_2")) GlobalData.UnlockLevel[2] = PlayerPrefs.GetInt("UnlockLevel_2");
        if (PlayerPrefs.HasKey("UnlockLevel_3")) GlobalData.UnlockLevel[3] = PlayerPrefs.GetInt("UnlockLevel_3");
        if (PlayerPrefs.HasKey("UnlockLevel_4")) GlobalData.UnlockLevel[4] = PlayerPrefs.GetInt("UnlockLevel_4");
        if (PlayerPrefs.HasKey("UnlockLevel_5")) GlobalData.UnlockLevel[5] = PlayerPrefs.GetInt("UnlockLevel_5");
        if (PlayerPrefs.HasKey("UnlockLevel_6")) GlobalData.UnlockLevel[6] = PlayerPrefs.GetInt("UnlockLevel_6");
        if (PlayerPrefs.HasKey("UnlockLevel_7")) GlobalData.UnlockLevel[7] = PlayerPrefs.GetInt("UnlockLevel_7");
        if (PlayerPrefs.HasKey("UnlockLevel_8")) GlobalData.UnlockLevel[8] = PlayerPrefs.GetInt("UnlockLevel_8");
        if (PlayerPrefs.HasKey("UnlockLevel_9")) GlobalData.UnlockLevel[9] = PlayerPrefs.GetInt("UnlockLevel_9");
        if (PlayerPrefs.HasKey("Stars")) GlobalData.Stars = PlayerPrefs.GetInt("Stars");
        if (PlayerPrefs.HasKey("Money")) GlobalData.Money = PlayerPrefs.GetInt("Money");
    }

    void CheckLevels()
    {
        for (int i = 0; i < GlobalData.UnlockLevel.Length; i++)
        {
            if (GlobalData.UnlockLevel[i] == 0)
            {
                Levels[i].SetActive(true);
                Levels[i].GetComponent<Image>().color = Color.gray;
                Levels[i].GetComponent<Image>().sprite = _lockImage;
                Levels[i].transform.parent.GetChild(0).gameObject.SetActive(false);
            }
            else {
                
                Levels[i].transform.parent.GetChild(0).gameObject.SetActive(true);
                switch (GlobalData.UnlockLevel[i])
                {
                    case 1:
                        Levels[i].SetActive(false);
                        Levels[i].transform.parent.GetChild(0).GetChild(0).GetComponent<Image>().sprite = _star;
                        Levels[i].transform.parent.GetChild(0).GetChild(2).GetComponent<Image>().sprite = _nullStar;
                        Levels[i].transform.parent.GetChild(0).GetChild(1).GetComponent<Image>().sprite = _nullStar;
                        break;
                    case 2:
                        Levels[i].SetActive(false);
                        Levels[i].transform.parent.GetChild(0).GetChild(2).GetComponent<Image>().sprite = _nullStar;
                        Levels[i].transform.parent.GetChild(0).GetChild(1).GetComponent<Image>().sprite = _star;
                        Levels[i].transform.parent.GetChild(0).GetChild(0).GetComponent<Image>().sprite = _star;
                        break;
                    case 3:
                        Levels[i].SetActive(false);
                        Levels[i].transform.parent.GetChild(0).GetChild(2).GetComponent<Image>().sprite = _star;
                        Levels[i].transform.parent.GetChild(0).GetChild(1).GetComponent<Image>().sprite = _star;
                        Levels[i].transform.parent.GetChild(0).GetChild(0).GetComponent<Image>().sprite = _star;
                        break;
                    case 4:
                        Levels[i].SetActive(true);
                        Levels[i].GetComponent<Image>().color = Color.white;
                        Levels[i].GetComponent<Image>().sprite = _openImage;
                        Levels[i].transform.parent.GetChild(0).gameObject.SetActive(false);
                     /*   Levels[i].transform.parent.GetChild(0).GetChild(2).GetComponent<Image>().sprite = _nullStar;
                        Levels[i].transform.parent.GetChild(0).GetChild(1).GetComponent<Image>().sprite = _nullStar;
                        Levels[i].transform.parent.GetChild(0).GetChild(0).GetComponent<Image>().sprite = _nullStar;*/
                        break;
                }
            }
        }
    }

    public void SelectLevel(int numberLevel)
    {
        if (GlobalData.UnlockLevel[numberLevel].Equals(0) || GlobalData.UnlockLevel[numberLevel].Equals(4) && numberLevel!=0)
        {
#region Price
            int price = 0;
            int stars = 0;
            switch (numberLevel)
            {
                case 0:
                    price = 0;
                    break;
                case 1:
                    price = 40;
                    stars = 2;
                    break;
                case 2:
                    price = 75;
                    stars = 4;
                    break;
                case 3:
                    price = 150;
                    stars = 6;
                    break;
                case 4:
                    price = 200;
                    stars = 8;
                    break;
                case 5:
                    price = 300;
                    stars = 12;
                    break;
                case 6:
                    price = 400;
                    stars = 15;
                    break;
                case 7:
                    price = 500;
                    stars = 20;
                    break;
                case 8:
                    price = 600;
                    stars = 24;
                    break;
                case 9:
                    price = 800;
                    stars = 27;
                    break;
            }
#endregion
            PricePopap.SetActive(true);
            PricePopap.transform.GetChild(0).FindChild("Logo").GetComponent<Text>().text = "Level " + (numberLevel + 1);
            PricePopap.transform.GetChild(0).FindChild("Price").GetComponent<Text>().text = GlobalData.Money.ToString() + "/" + price.ToString();
            PricePopap.transform.GetChild(0).FindChild("Stars").GetComponent<Text>().text = GlobalData.Stars.ToString() + "/" + stars.ToString();
            if (GlobalData.Money >= price) PricePopap.transform.GetChild(0).GetChild(2).GetChild(1).GetChild(0).gameObject.SetActive(true);
            else PricePopap.transform.GetChild(0).GetChild(2).GetChild(1).GetChild(0).gameObject.SetActive(false);
            if (GlobalData.Stars >= stars) PricePopap.transform.GetChild(0).GetChild(3).GetChild(1).GetChild(0).gameObject.SetActive(true);
            else PricePopap.transform.GetChild(0).GetChild(3).GetChild(1).GetChild(0).gameObject.SetActive(false);
            preNumer = numberLevel;
        }
        else {
          
            GlobalData.NumberLevel = numberLevel;
            if (GlobalData.NumberLevel == 0 || GlobalData.NumberLevel == 4 || GlobalData.NumberLevel == 9)
            {
                main.Buttons("comics");
            }
            else main.Buttons("Game");
        }
    }

    public void StartLevel()
    {
#region Price
        int price = 0;
        int stars = 0;
        switch (preNumer)
        {
            case 0:
                price = 0;
                break;
            case 1:
                price = 40;
                stars = 2;
                break;
            case 2:
                price = 75;
                stars = 4;
                break;
            case 3:
                price = 150;
                stars = 6;
                break;
            case 4:
                price = 200;
                stars = 8;
                break;
            case 5:
                price = 300;
                stars = 12;
                break;
            case 6:
                price = 400;
                stars = 15;
                break;
            case 7:
                price = 500;
                stars = 20;
                break;
            case 8:
                price = 600;
                stars = 24;
                break;
            case 9:
                price = 800;
                stars = 27;
                break;
        }
#endregion
        if (GlobalData.UnlockLevel[preNumer] == 0 || GlobalData.UnlockLevel[preNumer] == 4 && GlobalData.Money >= price && GlobalData.Stars >= stars)
        {
            GlobalData.UnlockLevel[preNumer] = 4;
            GlobalData.Money -= price;
            PlayerPrefs.SetInt("UnlockLevel_" + preNumer.ToString("0"), GlobalData.UnlockLevel[preNumer]);
            PlayerPrefs.SetInt("Money", GlobalData.Money);
            CheckLevels();
            PlayerPrefs.Save();
            GameObject _sun = Levels[preNumer].transform.parent.GetChild(3).gameObject;
            _sun.SetActive(true);
            StartCoroutine(GlobalData.SetActive(_sun, 1, false));
            GlobalData.NumberLevel = preNumer;
            if (GlobalData.NumberLevel == 0 || GlobalData.NumberLevel == 4 || GlobalData.NumberLevel == 9) {
                main.Buttons("comics");
            }
            else main.Buttons("Game");
        }
        else if (GlobalData.Stars < stars)
        {
            PricePopap.SetActive(false);
            _noMoney.SetActive(true);
            _noMoney.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "You need more stars...";
        }
        else if (GlobalData.Money < price)
        {
            PricePopap.SetActive(false);
            _noMoney.SetActive(true);
            _noMoney.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "No Money...";
        }
    }

    public void UseKeys() {
        if (GlobalData.Keys > 0)
        {
            GlobalData.UnlockLevel[preNumer] = 4;
            GlobalData.NumberLevel = preNumer;
            GlobalData.Keys -= 1;
            PlayerPrefs.SetInt("Keys", GlobalData.Keys);
            PlayerPrefs.SetInt("UnlockLevel_" + preNumer.ToString("0"), GlobalData.UnlockLevel[preNumer]);
            if (GlobalData.NumberLevel == 4 || GlobalData.NumberLevel == 9)
            {
                main.Buttons("comics");
            }
            else main.Buttons("Game");
        }
        else Shop.SetActive(true);
    }
}

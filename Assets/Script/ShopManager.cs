﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour {

    public Transform[] window;
    public Color _disableColor;
    public Text money;

    void Start() {
        ManageWindow(1);
    }


    void Update() {
        money.text = GlobalData.Money.ToString();
    }

    public void ManageWindow(int number) {
        switch (number) {
            case 1:
                window[0].SetAsLastSibling();
                break;
            case 2:
                window[1].SetAsLastSibling();
                break;
            case 3:
                window[2].SetAsLastSibling();
                break;
        }
        transform.GetChild(2).GetComponent<Image>().color = Color.white;
        transform.GetChild(1).GetComponent<Image>().color = _disableColor;
        transform.GetChild(0).GetComponent<Image>().color = _disableColor;
    }
}

﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public AudioSource[] Sound;
    public static bool muze;

    void Update() {
        SoundUpdate(GlobalData.SoundEnabled);
    }



    void SoundUpdate(bool _handle) {
        for (int i = 0; i < Sound.Length; i++) {
            if (_handle)
            {
                Sound[i].enabled = true;
            }
            else {
                Sound[i].enabled = false;
            }
        }
        
    }
}

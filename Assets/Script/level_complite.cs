﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AppodealAds.Unity.Api;

public class level_complite : MonoBehaviour {

    public GameObject[] _stars;
    public GameManager _gameManager;
    public int _nextStage;
    public Text TitleRoundMoney;
    public GameObject BonusButton;
    

    void Start() {
        StartCoroutine(StarInit(_procentTimerGame()));
        Debug.Log("NumberLevel: " + GlobalData.NumberLevel);
        _nextStage = GlobalData.NumberLevel + 1;
        TitleRoundMoney.text = "+ " + _gameManager.roundMoney.ToString();
        if (_gameManager.noHints && GlobalData.Achivment[0] == 0) {
            GlobalData.Achivment[0] = 1;
            GlobalData.Money += 20;
            _gameManager.ShowAchivment.SetActive(true);
            PlayerPrefs.SetInt("Achivment0", GlobalData.Achivment[0]);
            PlayerPrefs.SetInt("Money", GlobalData.Money);
        }
        if (_gameManager._timerAchivment < 15 && GlobalData.Achivment[1] == 0) {
            GlobalData.Achivment[1] = 1;
            GlobalData.Money += 100;
            _gameManager.ShowAchivment.SetActive(true);
            PlayerPrefs.SetInt("Achivment1", GlobalData.Achivment[1]);
            PlayerPrefs.SetInt("Money", GlobalData.Money);
        }
        if (_gameManager._timerGame <= 1 && GlobalData.Achivment[2] == 0) {
            GlobalData.Achivment[2] = 1;
            GlobalData.Money += 100;
            _gameManager.ShowAchivment.SetActive(true);
            PlayerPrefs.SetInt("Achivment2", GlobalData.Achivment[2]);
            PlayerPrefs.SetInt("Money", GlobalData.Money);
        }
    }

    float _procentTimerGame() {
        float _proc = _gameManager._timerGame * 100 / _gameManager._startTimerGame;
        return _proc;
    }

    IEnumerator StarInit(float _timer)
    {
        if (GlobalData.NumberLevel != 9)
        {
            if (GlobalData.UnlockLevel[GlobalData.NumberLevel + 1] == 0)
            {
                GlobalData.UnlockLevel[GlobalData.NumberLevel + 1] = 4;
                PlayerPrefs.SetInt("UnlockLevel_" + (GlobalData.NumberLevel + 1).ToString(), GlobalData.UnlockLevel[GlobalData.NumberLevel + 1]);
            }
        }
        if (_timer > 0 && _timer < 25)
        {
            yield return new WaitForSeconds(0.3f);
            _stars[0].transform.GetChild(0).gameObject.SetActive(true);
            if (GlobalData.UnlockLevel[GlobalData.NumberLevel] <= 1 || GlobalData.UnlockLevel[GlobalData.NumberLevel] == 4)
            {
                GlobalData.UnlockLevel[GlobalData.NumberLevel] = 1;
                GlobalData.Stars += 1;
                PlayerPrefs.SetInt("Stars", GlobalData.Stars);
            }
            PlayerPrefs.SetInt("UnlockLevel_" + GlobalData.NumberLevel, GlobalData.UnlockLevel[GlobalData.NumberLevel]);
        }
        else if (_timer >= 25 && _timer < 50)
        {
            yield return new WaitForSeconds(0.4f);
            _stars[0].transform.GetChild(0).gameObject.SetActive(true);
            _stars[1].transform.GetChild(0).gameObject.SetActive(true);
            if (GlobalData.UnlockLevel[GlobalData.NumberLevel] <= 2 || GlobalData.UnlockLevel[GlobalData.NumberLevel] == 4)
            {
                switch (GlobalData.UnlockLevel[GlobalData.NumberLevel])
                {
                    case 1:
                        GlobalData.Stars += 1;
                        break;
                    case 4:
                        GlobalData.Stars += 2;
                        break;
                }
                GlobalData.UnlockLevel[GlobalData.NumberLevel] = 2;
                PlayerPrefs.SetInt("Stars", GlobalData.Stars);
            }
            PlayerPrefs.SetInt("UnlockLevel_" + GlobalData.NumberLevel, GlobalData.UnlockLevel[GlobalData.NumberLevel]);
        }
        else if (_timer >= 50)
        {
            yield return new WaitForSeconds(0.5f);
            _stars[0].transform.GetChild(0).gameObject.SetActive(true);
            _stars[1].transform.GetChild(0).gameObject.SetActive(true);
            _stars[2].transform.GetChild(0).gameObject.SetActive(true);
            if (GlobalData.UnlockLevel[GlobalData.NumberLevel] <= 3 || GlobalData.UnlockLevel[GlobalData.NumberLevel] == 4)
            {
                switch (GlobalData.UnlockLevel[GlobalData.NumberLevel]) {
                    case 1:
                        GlobalData.Stars += 2;
                        break;
                    case 2:
                        GlobalData.Stars += 1;
                        break;
                    case 4:
                        GlobalData.Stars += 3;
                        break;
                }
                GlobalData.UnlockLevel[GlobalData.NumberLevel] = 3;
                PlayerPrefs.SetInt("Stars", GlobalData.Stars);
            }
            PlayerPrefs.SetInt("UnlockLevel_" + GlobalData.NumberLevel, GlobalData.UnlockLevel[GlobalData.NumberLevel]);
        }
        
        PlayerPrefs.Save();
    }

    public void RewardedVideo(bool show)
    {
        if (show)
        {
            Appodeal.show(Appodeal.REWARDED_VIDEO);
        }
        else {
            GlobalData.Money += _gameManager.roundMoney;
            _gameManager.roundMoney = _gameManager.roundMoney * 2;
            TitleRoundMoney.text = "+ " + _gameManager.roundMoney.ToString();
            BonusButton.SetActive(false);
        }
    }
}
